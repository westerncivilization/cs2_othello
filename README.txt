Contributions

Week 1:
James did most of the actual coding, since he started looking at the code before Perren did and there wasnt much code to write. In person, we discussed the main strategy we would use for our AI of the week, wrote the code, sorted out minor issues we ran into, and submitted the set.

Week 2:
Perren tried to reduce CheckMove calls (990635f) but his method was flawed and this was reverted (0ce6798).  James added a simple look-ahead recursion scheme with adjustable recursion depth, which works and sometimes make super awesome comebacks in the last few moves (6f39514).
In person, we added preference for edges and corners to this new scheme.  The README was updated [pending commit].

----------------------------------------
Improvement Log

Week 1 (baseline):
The AI iterates through all moves available to it, weighting each possible move by the number of new captures it makes.  Edge moves are preferred, so they count as triple the captures; corners are highly preferred, to the extent that when available they are examined in their own priority tier. The first move that has the maximized score-per-turn in this scheme is chosen.

Week 2:
Testing last week's AI in 25 games as black and 25 games as white against ConstantTimePlayer: 5/20 (big loss), 13/1/11 (almost tie, slight loss).
After adding simple recursive look-ahead (6f39514) and setting the depth to 5, the AI appears to generally beat ConstantTimePlayer more than half the time.
After seeing the above change do not-as-awesomely-as-we-hoped, we once again weighted corners by a full tier, and also weighted edges as +3 stones.  Except at the end of the game (7 moves left).
