#include "exampleplayer.h"

/*
 * Constructor for the player; initialize everything here. The side your AI is
 * on (BLACK or WHITE) is passed in as "side". The constructor must finish 
 * within 30 seconds.
 */
ExamplePlayer::ExamplePlayer(Side side) {
    board = new Board();
	mySide = side;
	otherSide = (side == BLACK) ? WHITE : BLACK;
}

/*
 * Destructor for the player.
 */
ExamplePlayer::~ExamplePlayer() {
	delete board;
}

/*
 * Flattens x, y coordinates into a single integer in our 64 bit field
 */
int ExamplePlayer::flatten(int x, int y) {
	return x + BOARD_SIZE * y;
} 

/*
 * Compute the next move given the opponent's last move. Each AI is
 * expected to keep track of the board on its own. If this is the first move,
 * or if the opponent passed on the last move, then opponentsMove will be NULL.
 *
 * If there are no valid moves for your side, doMove must return NULL.
 *
 * Important: doMove must take no longer than the timeout passed in 
 * msLeft, or your AI will lose! The move returned must also be legal.
 */
Move *ExamplePlayer::doMove(Move *opponentsMove, int msLeft) {
    //update our board state w/ opponentsMove
	board->doMove(opponentsMove, otherSide);
	
	//generate a list of all valid moves with value
	float maxValue = -1;
	int maxX = -1;
	int maxY = -1;
	int stones = board->count(mySide);
	
	for (int i = 0; i < BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {
            Move move(i, j);
            //if the move is valid, test it on a copy of the board
			if (board->checkMove(&move, mySide)){
				Board *tboard = board->copy();
				tboard->doMove(&move, mySide);
				int captures = tboard->count(mySide) - stones;
				if(((i == 0) || (i == 7)) && ((j == 0) || (j == 7)))
				{
					captures *= 100;
				}
				else if(((i == 0) || (i == 7)) || ((j == 0) || (j == 7)))
				{
					captures *= 3;
				}
				//compare value of move to current max value
				if(captures > maxValue)
				{
					maxValue = captures;
					maxX = i;
					maxY = j;
				}
			}
        }
    }
	//End finding best move
	
	//if we havn't updated maxValue, we have no valid moves
	if(maxValue == -1) return NULL;
	//update board and return bestMove
	Move *bestMove = new Move(maxX, maxY);
	board->doMove(bestMove, mySide);
	return(bestMove);
}
    
