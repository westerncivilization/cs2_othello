#ifndef __EXAMPLEPLAYER_H__
#define __EXAMPLEPLAYER_H__

#include <iostream>
#include "common.h"
#include "board.h"
using namespace std;

class ExamplePlayer {

private:
Side mySide;
Side otherSide;

Board *board;
int flatten (int x, int y);

int lookAhead (Board *tboard, int depth, Side side);

public:
    ExamplePlayer(Side side);
    ~ExamplePlayer();
    
    Move *doMove(Move *opponentsMove, int msLeft);
};

#endif
